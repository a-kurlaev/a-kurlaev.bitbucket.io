window.addEventListener('load', inputApp);

function inputApp() {
  const imageIndex = Math.floor(Math.random() * 3);
  selectImage(imageIndex);
}

function selectImage(imageIndex) {
  imageTag = document.getElementsByClassName('about-program__header-image-wrapper')[0];
  if (imageIndex == 0) {
    imageIndex = '';
  }
  imageTag.innerHTML = '          <img src="images/eats' + imageIndex + '.jpg" class="about-program__header-image">'
}