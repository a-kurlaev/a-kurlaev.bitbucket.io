window.addEventListener('load', createTable);

function createTable() {
  const propertyNames = Object.keys(window.foodProperties);
  const foodNames = Object.keys(window.foods);
  let columnObjects = [];
  columnObjects.push(createHeaderColumn(foodNames));
  createColumnData(columnObjects, foodNames, propertyNames);
  renderDayResultsTable(columnObjects, foodNames);
}

function createHeaderColumn(foodNames) {
  const column = [{
    value: 'Продукт',
    style: 'table__column-header', 
  }];
  
  for (let i = 0; i < foodNames.length; i++) {
    const name = foodNames[i];
    const food = window.foods[name];
    column.push({
      value: food.title,
      style: 'table__row-header'
    });
  }
  
  return column;
}

function createColumnData(columnObjects, foodNames, propertyNames) {
  for (let i = 0; i < propertyNames.length; i++) {
    const propertyName = propertyNames[i];
    let column = [
      {
        value: window.foodProperties[propertyName].title,
        style: 'table__column-header'
      }
    ];
    for (let j = 0; j < foodNames.length; j++) {
      food = foodNames[j];
      let cell = {
        value: window.foods[food].properties[propertyName],
        style: ''
      }
      column.push(cell);
    }
    columnObjects.push(column);
  }
  let g = 0;
}

function renderDayResultsTable(columnObjects, foodNames) {
  let tableHtml = '';
  const rowNumbers = foodNames.length + 1;
  
  for (let rowIndex = 0; rowIndex < rowNumbers; rowIndex++) {
    let rowHtml = '';
    
    for (let columnIndex = 0; columnIndex < columnObjects.length; columnIndex++) {
      const cell = columnObjects[columnIndex][rowIndex];
      let tagName = 'td';
      
      if (columnIndex == 0 || rowIndex == 0) {
        tagName = 'th';
      }
      rowHtml += '            <' + tagName + ' class="composition-table__cell ' + cell.style + '">' + cell.value + '</' + tagName + '>\n';
    }
    
    tableHtml += '          <tr>' + rowHtml + '</tr>\n';
  }
  
  let tableTag = document.getElementsByClassName('js-day-results-table')[0];
  tableTag.innerHTML = tableHtml;
}

