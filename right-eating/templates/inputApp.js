window.inputAppTemplates = {
  mealtimeFood: `
            <div class="mealtime__food">
              <div class="mealtime__food-image-wrapper">
                <img class="mealtime__food-image" src="{{FOOD_IMAGE}}"/>
              </div>
              <div class="mealtime__food-description">
                <div class="mealtime__food-name">
                  {{FOOD_NAME}}
                </div>
                <div class="mealtime__food-amount-container">
                  <input type="number" class="mealtime__food-amount" value="{{FOOD_AMOUNT}}" data-food-id="{{FOOD_ID}}"/>
                  <span class="mealtime__food-amount-unit">
                    {{FOOD_AMOUNT_UNIT}}
                  </span>
                </div>
              </div>
            </div> 
  `,
  message: `
          <div class="message">
            {{MESSAGE}}
          </div>
  `
};
