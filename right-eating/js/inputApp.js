window.addEventListener('load', inputApp);

function addUserButtonClick() {
  const userContainerTag = document.getElementsByClassName('input-container__username-container')[0];
  userContainerTag.classList.add('input-container__username-container_new-user');
}

function cancelUserButtonClick() {
  const userContainerTag = document.getElementsByClassName('input-container__username-container')[0];
  const userInputTag = document.getElementsByClassName('input-container__new-username')[0];
  userInputTag.value = '';
  userContainerTag.classList.remove('input-container__username-container_new-user');
}

function changeOnInputDate() {
  let day = Number(document.getElementsByClassName('input-date__day')[0].value);
  let month = Number(document.getElementsByClassName('input-date__month')[0].value);
  let year = Number(document.getElementsByClassName('input-date__year')[0].value);
  
  day = checkDate(day, month, year);
  
  let date = dateGenerator(day, month, year);
  
  fillDate(date); 
  
  const userName = getUserName();
  let data = readFromDB(userName);
  
  fillMealtimeContainer(data, date);
  
  renderMessagesContainer(data);
}

function checkDate(day, month, year) {
  months = [0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
  
  if (year % 4 == 0) {
    months[2] = 29;
  }
  
  if (months[month] < day) {
    day = months[month];
  }
  
  if (day < 1) {
    day = 1;
  }
  
  return day;
}

function inputApp() {
  fillUserSelect();
  
  const userName = getUserName();
  let data = readFromDB(userName);
  
  fillGenderSelect(data);
  
  let today = getToday();
  
  fillDate(today);
  
  fillMealtimeContainer(data, today);
  
  renderMessagesContainer(data);
  
  subscribeEventListeners();
}

function fillUserSelect() {
  let usersList = Object.keys(localStorage);
  
  if (usersList.length == 0) {
    const userContainerTag = document.getElementsByClassName('input-container__username-container')[0];
    userContainerTag.classList.add('input-container__username-container_new-user');
  }
  
  let html = '<option disabled>Выберите имя:</option>';
  let userSelectTag = document.getElementsByClassName("input-container__username")[0];
  
  for (let i = 0; i < usersList.length; i++) {
    let selecting = '';
    
    if (userSelectTag.value ==  usersList[i]) {
      selecting = ' selected';
    }
    
    html += '<option value="' + usersList[i] + '"' + selecting + '>' + usersList[i] + '</option>';
  }
  
  userSelectTag.innerHTML = html;
}

function subscribeEventListeners() {
  document.getElementsByClassName('input-container__username')[0].addEventListener('change', inputApp);
  document.getElementsByClassName('input-container__new-username')[0].addEventListener('change', inputApp);
  document.getElementsByClassName('input-container__add-username')[0].addEventListener('click', addUserButtonClick);
  document.getElementsByClassName('input-container__cancel-username')[0].addEventListener('click', cancelUserButtonClick);
  document.getElementsByClassName('mealtime-save__button')[0].addEventListener('click', saveButtonClick);
  document.getElementsByClassName('input-date__day')[0].addEventListener('change', changeOnInputDate);
  document.getElementsByClassName('input-date__month')[0].addEventListener('change', changeOnInputDate);
  document.getElementsByClassName('input-date__year')[0].addEventListener('change', changeOnInputDate);
}

function getToday() {
  let today = new Date();
  
  return dateGenerator(today.getDate(), today.getMonth() + 1, today.getFullYear());
}

function dateGenerator(day, month, year) {
  return {
    day: Number(day),
    month: Number(month),
    year: Number(year),
    toString: function () {
      return this.year + '-' + this.month + '-' + this.day;
    }
  };
}

function mealtimeListsGenerator() {
  return {
    breakfastHome: [],
    breakfastSchool: [],
    lunch: [],
    dinner: []
  };
}

function fillGenderSelect(data) {
  let gender = data.gender;
  
  if (gender == undefined) {
    gender = "boy";
  }
  
  const genderSelectTag = document.getElementsByClassName('input-container__gender-value')[0];
  genderSelectTag.value = gender;
}

function fillDate(today) {
  const dayTag = document.getElementsByClassName('input-date__day')[0];
  dayTag.value = today.day;
  const monthTag = document.getElementsByClassName('input-date__month')[0];
  monthTag.value = today.month;
  const yearTag = document.getElementsByClassName('input-date__year')[0];
  yearTag.value = today.year;
}

function getUserName() {
  const userContainerTag = document.getElementsByClassName('input-container__username-container')[0];
  let userName;
  
  if (userContainerTag.classList.contains('input-container__username-container_new-user')) {
    userName = document.getElementsByClassName('input-container__new-username')[0].value; 
  } else {
    userName = document.getElementsByClassName('input-container__username')[0].value;
  }
  
  return userName || '';
}

function readFromDB(fileName) {
  // TODO: Добавить обработку исключений
  let objectAsString = localStorage.getItem(fileName);
  
  if (objectAsString == null) {
    objectAsString = '{}';
  }
  
  return JSON.parse(objectAsString);
}

function writeToDB(fileName, data) {
  const objectAsString = JSON.stringify(data);
  
  localStorage.setItem(fileName, objectAsString);
}

function fillMealtimeContainer(data, today) {
  const mealtimeLists = createMealtimeLists();

  fillAmountInLists(mealtimeLists, today, data);
  
  for (let i = 0; i < mealtimeLists.length; i++) {
    mealtimeLists[i].sort(mealtimeComparator);
  }
  
  renderToMealtimeContainer(mealtimeLists);
  
  fillDayResultsTable(mealtimeLists);
}

function createMealtimeLists() {
  const mealtimes = [ [], [], [], [] ];
  const foodPropertyNames = Object.keys(window.foods);
  
  for (let i = 0; i < foodPropertyNames.length; i++) {
    const key = foodPropertyNames[i];
    const food = window.foods[key];
    
    for (let j = 0; j < food.whenEating.length; j++) {
      if (food.whenEating[j] == true) {
        mealtimes[j].push({
          foodId: key,
          title: food.title,
          unit: food.unit,
          image: food.image
        });
      }
    }
  }
  
  return mealtimes;
}

function fillAmountInLists(mealtimeLists, today, data){
  let daysData = data.data;
  
  if (daysData == undefined) {
    daysData = {};
  }
  
  let dayData = daysData[today.toString()];
  
  if (dayData == undefined) {
    dayData = mealtimeListsGenerator();
  }
  
  fillAmountInOneList(mealtimeLists[0], dayData.breakfastHome);
  fillAmountInOneList(mealtimeLists[1], dayData.breakfastSchool);
  fillAmountInOneList(mealtimeLists[2], dayData.lunch);
  fillAmountInOneList(mealtimeLists[3], dayData.dinner);
}

function fillAmountInOneList(fullList, savedList) {
  for (let i = 0; i < savedList.length; i++) {
    const foodElement = savedList[i];
    const foodObject = findObjectById(fullList, foodElement.foodId);

    if (foodObject == null) {
      continue;
    }

    foodObject.amount = foodElement.amount;
  }
}

function findObjectById(list, foodId) {
  for (let i = 0; i < list.length; i++) {
    if (list[i].foodId == foodId) {
      return list[i];
    }
  }

  return null;
}

function mealtimeComparator(o1, o2) {
  let amount1 = 2;
  let amount2 = 2;
  
  if (o1.amount != undefined) {
    amount1 = 1;
  }
  
  if (o2.amount != undefined) {
    amount2 = 1;
  }
  
  if (amount1 == amount2) {
    let title1 = o1.title.toLowerCase();
    let title2 = o2.title.toLowerCase();
    
    if (title1 < title2) {
      return -1;
    } else if (title1 > title2) {
      return 1;
    } else {
      return 0;
    }
  } else {
    return amount1-amount2;
  }
}

function renderToMealtimeContainer(mealtimeLists) {
  const foodListTags = document.getElementsByClassName('mealtime__food-list');
  
  for (let i = 0; i < foodListTags.length; i++) {
    let html = '';
    let mealtimeList = mealtimeLists[i];
    
    for (let j = 0; j < mealtimeList.length; j++) {
      const food = mealtimeList[j];
      let foodHtml = window.inputAppTemplates.mealtimeFood;
      foodHtml = foodHtml.replace('{{FOOD_IMAGE}}', 'images/' + food.image);
      foodHtml = foodHtml.replace('{{FOOD_NAME}}', food.title);
      foodHtml = foodHtml.replace('{{FOOD_AMOUNT}}', food.amount || '');
      foodHtml = foodHtml.replace('{{FOOD_AMOUNT_UNIT}}', food.unit);
      foodHtml = foodHtml.replace('{{FOOD_ID}}', food.foodId);
      html += foodHtml;
    }
    
    foodListTags[i].innerHTML = html;
  }
}

function saveButtonClick() {
  const userName = getUserName();
  
  if (userName == '') {
    alert('Чтобы сохранить данные введите имя пользователя и снова нажмите "Сохранить".');
  
    return;
  }
  
  cancelUserButtonClick();
  
  const listTags = document.getElementsByClassName('mealtime__food-list');
  const mealtimeLists = [[], [], [], []];
  
  for (let i = 0; i < listTags.length; i++) {
    const tags = listTags[i].getElementsByClassName('mealtime__food-amount');
    
    for (let j = 0; j < tags.length; j++) {
      let amount = Number(tags[j].value);
      
      let mealtimes = ['Завтрак дома', 'Завтрак в школе', 'Обед', 'Ужин']
      
      if (amount < 0) {
        alert("Введите для '" + window.foods[tags[j].dataset.foodId].title + "' на '" + mealtimes[i] + "' неотрицательное число")
      }
      
      if (amount > 0) {
        mealtimeLists[i].push({
          foodId: tags[j].dataset.foodId,
          amount: amount 
        })
      }
    }
  }
  
  let weekData = {};
  let foodsNorms = Object.keys(window.foodNorms);
  for (let i = 0; i < foodsNorms.length; i++) {
    weekData[foodsNorms[i]] = {
      name: window.foodNorms[foodsNorms[i].name],
      amount: 0
    }
  }
  const typeNames = getTypeNames();
  
  fillWeekData(mealtimeLists, weekData, typeNames, userName);
  
  saveToLocalStorage(mealtimeLists, weekData, typeNames, userName);
  
  fillUserSelect();
  
  let data = readFromDB(userName);
  
  renderMessagesContainer(data);
  
  fillDayResultsTable(mealtimeLists);
}

function getTypeNames() {
  return Object.keys(window.foodNorms);
}

function getWeekData(data) {
  let day = Number(document.getElementsByClassName('input-date__day')[0].value);
  let month = Number(document.getElementsByClassName('input-date__month')[0].value);
  let year = Number(document.getElementsByClassName('input-date__year')[0].value);
  day = checkDate(day, month, year);
  let now = new Date(year, month - 1, day, 0, 0, 0);
  let weekDay = (now.getDay() + 7 - 1) % 7;
  let mondayDay = day - weekDay;
  let weekData = {};
  const foodNames = Object.keys(window.foods);
  
  for (let i = 0; i < 7; i++) {
    let currentDate = new Date(year, month - 1, mondayDay + i);
    day = currentDate.getDate();
    month = currentDate.getMonth() + 1;
    year = currentDate.getFullYear();
    
    let date = dateGenerator(day, month, year);
    data.data = data.data || {};
    data.data[date] = data.data[date] || {};
    const mealtimes = ['breakfastHome', 'breakfastSchool', 'lunch', 'dinner'];
    
    for (let j = 0; j < mealtimes.length; j++) {
      let mealtime = mealtimes[j];
      let mealtimeList = data.data[date][mealtime] || [];
      
      for (let k = 0; k < mealtimeList.length; k++) {
        let food = mealtimeList[k];
        let typeName = window.foods[food.foodId].type;
        weekData[typeName] = weekData[typeName] || {};
        weekData[typeName].amount = weekData[typeName].amount || 0;
        weekData[typeName].amount += food.amount;
      }
    }
  }
  
  return weekData;
}

function fillWeekData(mealtimeLists, weekData, typeNames, userName) {
  let date = getDateFromDom();
  let data = readFromDB(userName);
  for (let i = 0; i < mealtimeLists.length; i++) {
    const mealtimeList = mealtimeLists[i];
    if (mealtimeList == []) {
      continue;
    }
    for (let j = 0; j < mealtimeList.length; j++) {
      let foodTypesFromLocalStorage = {};
      
      for (let k = 0; k < typeNames.length; k++) {
        foodTypesFromLocalStorage[typeNames[k]] = 0;
      }
      
      const food = mealtimeList[j];
      const foodType = foods[food.foodId].type;
      const mealtimes = ['breakfastHome', 'breakfastSchool', 'lunch', 'dinner'];
      
      for (let k = 0; k < mealtimes.length; k++) {
        let mealtime = mealtimes[k];
        
        if (data.data == undefined) {
          continue;
        }
        
        if (data.data[date] == undefined) {
          continue;
        }
        
        for (let l = 0; l < data.data[date][mealtime].length; l++) {
          let foodType = window.foods[data.data[date][mealtime][l].foodId].type;
          foodTypesFromLocalStorage[foodType] += data.data[date][mealtime][l].amount;
        }
        
      }
      
      weekData[foodType] = weekData[foodType] || {};
      weekData[foodType].amount = weekData[foodType].amount || 0;
      weekData[foodType].amount += Number(food.amount) - foodTypesFromLocalStorage[foods[food.foodId].type];
    }
  }

}

function saveToLocalStorage(mealtimeLists, weekData, typeNames, userName) {
  const gender = getGenderFromSelect();
  const date = getDateFromDom();
  const dateAsString = date.toString();
  let data = readFromDB(userName);
  
  data.gender = gender;
  data.data = data.data || {};
  data.data[dateAsString] = data.data[dateAsString] || mealtimeListsGenerator();
  data.data[dateAsString].breakfastHome = mealtimeLists[0];
  data.data[dateAsString].breakfastSchool = mealtimeLists[1];
  data.data[dateAsString].lunch = mealtimeLists[2];
  data.data[dateAsString].dinner = mealtimeLists[3];
  
  writeToDB(userName, data);
}

function renderMessagesContainer(data) {
  const deviation = fillDeviation(data);
  let messages = {};
  createMessages(deviation, messages);
  let html = '';
  let messageList = document.getElementsByClassName('messages-container')[0];
  const typeNames = getTypeNames();
  
  for (let i = 0; i < typeNames.length; i++) {
    let typeName = typeNames[i];
    if (messages[typeName] == undefined) {
      continue
    }
    let message = window.inputAppTemplates.message;
    message = message.replace('{{MESSAGE}}', messages[typeName] + '. Норма превышена на ' + deviation[typeName] + ' г');
    html += message;
  }
  
  const method = html ? 'add' : 'remove';
  document.getElementsByClassName('messages-container')[0].classList[method]('messages-container_visible');
  
  messageList.innerHTML = html;
}

function createMessages(deviation, messageList) {
  const messages = {
    milk: 'Вы выпили достаточно молока за неделю за неделю. Советуем вам больше его не пить',
    cottageCheese: 'Вы съели достаточно творога за неделю. Советуем вам больше его не есть',
    sourCream: 'Вы съели достаточно сметаны за неделю. Советуем вам больше её не есть',
    cheese: 'Вы съели достаточно сыра за неделю. Советуем вам больше его не есть',
    meat: 'Вы съели достаточно мяса за неделю. Советуем вам больше его не есть',
    fish: 'Вы съели достаточно рыбы за неделю. Советуем вам больше её не есть',
    egg: 'Вы съели достаточно яиц за неделю. Советуем вам больше их не есть',
    ryeBread: 'Вы съели достаточно ржаного хлеба за неделю. Советуем вам больше его не есть',
    wheatBread: 'Вы съели достаточно пшеничного хлеба за неделю. Советуем вам больше его не есть',
    pasta: 'Вы съели достаточно макаронных изделий за неделю. Советуем вам больше их не есть',
    cereals: 'Вы съели достаточно круп за неделю. Советуем вам больше их не есть',
    legumes: 'Вы съели достаточно бобовых за неделю. Советуем вам больше их не есть',
    confectionerySugar: 'Вы съели достаточно сладкого за неделю. Советуем вам больше его не есть',
    animalFats: 'Вы съели достаточно животных жиров за неделю. Советуем вам больше их не есть',
    potatoes: 'Вы съели достаточно картофеля за неделю. Советуем вам больше его не есть',
    vegetables: 'Вы съели достаточно овощей за неделю. Советуем вам больше их не есть',
    fruits: 'Вы съели достаточно фруктов за неделю. Советуем вам больше их не есть',
    berries: 'Вы съели достаточно ягод за неделю. Советуем вам больше их не есть'
  };
  const deviationNames = Object.keys(deviation);
  let typeNames = Object.keys(deviation);
  
  for (let i = 0; i < deviationNames.length; i++) {
    let typeName = typeNames[i];
    messageList[typeName] = messages[typeName];
  }
}

function fillDeviation(data) {
  let deviation = {};
  const typeNames = getTypeNames();
  data.data = data.data || {};
  const weekData = getWeekData(data);
  
  for (let i = 0; i < typeNames.length; i++) {
    const typeName = typeNames[i];
    weekData[typeName] = weekData[typeName] || {};
    weekData[typeName].amount = weekData[typeName].amount || 0;
    
    if (foodNorms[typeName].norm < weekData[typeName].amount) {
      deviation[typeName] = weekData[typeName].amount - foodNorms[typeName].norm;
    }
  }
  
  return deviation;
}

function getGenderFromSelect() {
  const genderSelectTag = document.getElementsByClassName('input-container__gender-value')[0];
  return genderSelectTag.value;
}

function getDateFromDom() {
  const day = document.getElementsByClassName('input-date__day')[0].value;
  const month = document.getElementsByClassName('input-date__month')[0].value;
  const year = document.getElementsByClassName('input-date__year')[0].value;
  return dateGenerator(day, month, year);
}

function fillDayResultsTable(mealtimeLists) {
  const propertyNames = [
    'protein', 'fat', 'carbohydrate', 
    'Na', 'K', 'Ca', 'Mg', 'P', 'Fe', 
    'A', 'beta', 'B1', 'B2', 'PP', 'C', 
    'energy'
  ];
  const columnObjects = [];
  
  let result = true;
  
  columnObjects.push(createHeaderColumn(propertyNames));
  
  createColumnData(propertyNames, mealtimeLists, columnObjects);
  
  createResultColumn(columnObjects, propertyNames);
  
  createNormColumn(columnObjects, propertyNames);
  
  result = createDeviationColumn(columnObjects, propertyNames, result);
  
  renderDayResultsTable(columnObjects, propertyNames);
  
  renderDayResult(result);
}

function createHeaderColumn(propertyNames) {
  const column = [{
    value: 'Приём пищи',
    style: 'table__column-header', 
  }];
  
  for (let i = 0; i < propertyNames.length; i++) {
    const name = propertyNames[i];
    const foodProperty = window.foodProperties[name];
    column.push({
      value: foodProperty.title + ', ' + foodProperty.unit,
      style: 'table__row-header'
    });
  }
  
  return column;
}

function createColumnData(propertyNames, mealtimeLists, columnObjects) {
  const titles = [{
    value: 'Завтрак дома',
    style: 'table__column-header'
  },
  {
    value: 'Завтрак в школе',
    style: 'table__column-header'
  },
  {
    value: 'Обед',
    style: 'table__column-header'
  },
  {
    value: 'Ужин',
    style: 'table__column-header'
  }];
  const gender = getGenderFromSelect();
  const foods = window.foods;
  
  for (let i = 0; i < titles.length; i++) {
    let column = [titles[i]];
    const mealtimeList = mealtimeLists[i];
    let propertyAmounts = {}
    
    for (let j = 0; j < propertyNames.length; j++) {
      propertyAmounts[propertyNames[j]] = 0;
    }
    
    for (let j = 0; j < mealtimeList.length; j++) {
      const foodAmount = mealtimeList[j].amount;
      
      if (!foodAmount) {
        continue;
      }
      
      const foodName = mealtimeList[j].foodId;
      const food = foods[foodName];
      
      for (let k = 0; k < propertyNames.length; k++) {
        let propertyName = propertyNames[k];
        const foodProperty = food.properties[propertyName];
        propertyAmounts[propertyName] += foodAmount / 100 * foodProperty;
        propertyAmounts[propertyName] = propertyAmounts[propertyName].toFixed(2) * 1.0;
      }
    }

    for (let j = 0; j < propertyNames.length; j++) {
      column.push({
        value: propertyAmounts[propertyNames[j]],
        style: ''
      });
    }
    
    
    columnObjects.push(column)
  }
}

function createResultColumn(columnObjects, propertyNames) {
  let column = [{
    value: 'Всего за день',
    style: 'table__column-header'
  }];
  
  for (let rowIndex = 1; rowIndex < propertyNames.length + 1; rowIndex++) {
    let foodPropertyName = propertyNames[rowIndex];
    let foodProperty = {
      value: 0,
      style: ''
    };
    
    for (let columnIndex = 1; columnIndex < columnObjects.length; columnIndex++) {
      let cellValue = Number(columnObjects[columnIndex][rowIndex].value);
      foodProperty.value += cellValue;
    }
    
    foodProperty.value = foodProperty.value.toFixed(2) * 1.0;
    column.push(foodProperty);
  }
  
  columnObjects.push(column);
}

function createNormColumn(columnObjects, propertyNames) {
  const gender = getGenderFromSelect();
  let column = [{
    value: 'Дневная норма',
    style: 'table__column-header'
  }];
  
  for (let rowIndex = 0; rowIndex < propertyNames.length; rowIndex++) {
    const foodPropertyName = propertyNames[rowIndex];
    let foodProperty = {
      value: window.foodProperties[foodPropertyName].norm[gender],
      style: ''
    };
    column.push(foodProperty);
  }
  
  columnObjects.push(column);
}

function createDeviationColumn(columnObjects, propertyNames, result) {
  let column = [{
    value: 'Отклонения от нормы',
    style: 'table__column-header'
  }];
  let resultColumn = columnObjects[columnObjects.length - 2];
  let normColumn = columnObjects[columnObjects.length - 1];
  
  for (let rowIndex = 0; rowIndex < propertyNames.length; rowIndex++) {
    let deviationValue = resultColumn[rowIndex + 1].value - normColumn[rowIndex + 1].value;
    let style = 'result';
    let epsilon = normColumn[rowIndex + 1].value / 100 * 15;
    
    if (deviationValue < -1 * epsilon || deviationValue > epsilon) {
      style = 'result_with-deviation';
      result = false;
    }
    
    deviationValue = deviationValue.toFixed(2);
    let deviation = {
      value: deviationValue,
      style: style
    };
    
    column.push(deviation);
  }
  
  columnObjects.push(column);
  return result;
}

function renderDayResultsTable(columnObjects, propertyNames) {
  let tableHtml = '';
  const rowNumbers = propertyNames.length + 1;
  
  for (let rowIndex = 0; rowIndex < rowNumbers; rowIndex++) {
    let rowHtml = '';
    
    for (let columnIndex = 0; columnIndex < columnObjects.length; columnIndex++) {
      const cell = columnObjects[columnIndex][rowIndex];
      let tagName = 'td';
      
      if (columnIndex == 0 || rowIndex == 0) {
        tagName = 'th';
      }
      rowHtml += '            <' + tagName + ' class="table__cell ' + cell.style + '">' + cell.value + '</' + tagName + '>\n';
    }
    
    tableHtml += '          <tr>' + rowHtml + '</tr>\n';
  }
  
  let tableTag = document.getElementsByClassName('js-day-results-table')[0];
  tableTag.innerHTML = tableHtml;
}

function renderDayResult(result) {
  let resultTag = document.getElementsByClassName('result-container')[0];
  let html = '';
  let style = 'result';
  if (result) {
    html = 'Все нормы соблюдены';
  } else {
    style = "result_with-deviations";
    html = 'Не все нормы соблюдены. Обратите внимание на таблицу.';
  }
  html = '<div class="' + style + '">' + html + '</div>';
  resultTag.innerHTML = html;
}