initFoodNorms();

function initFoodNorms() {
  window.foodNorms = {
    milk:{
      name: 'Молоко',
      norm: 3500
    },
    cottageCheese: {
      name: 'Творог',
      norm: 315
    },
    sourCream: {
      name: 'Сметана' ,
      norm: 105
    },
    cheese: {
      name: 'Сыр',
      norm: 70
    },
    meat: {
      name: 'Мясо',
      norm: 1225
    },
    fish: {
      name: 'Рыба',
      norm: 420
    },
    egg: {
      name: 'Яйца',
      norm: 350
    },
    ryeBread: {
      name: 'Хлеб ржаной',
      norm: 700
    },
    wheatBread: {
      name: 'Хлеб пшеничный',
      norm: 1400
    },
    pasta: {
      name: 'Макаронные изделия',
      norm: 105
    },
    cereals: {
      name: 'Крупы',
      norm: 210
    },
    legumes: {
      name:'Бобовые' ,
      norm: 70
    },
    confectionerySugar: {
      name: 'кондитерские изделия и сахар',
      norm: 525
    },
    animalFats: {
      name: 'Жиры животные',
      norm: 175
    },
    potatoes: {
      name: 'Картофель',
      norm:	1750
    },
    vegetables: {	
      name: 'Овощи',
      norm: 2100
    },
    fruits: {
      name: 'Фрукты',
      norm: 1050//-3500
    },
    berries: {
      name: 'Ягоды' ,
      norm: 350//-700
    }
  }
}