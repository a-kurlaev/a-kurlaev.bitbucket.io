// Перед этим модулем должен быть подключен модуль units.js

initFoodProperties();

function initFoodProperties() {
  const {g, mg, mcg, kkal} = units;

  window.foodProperties = {
    protein: {
      title: 'Белки',
      unit: g,
      norm: {
        boy: 93,
        girl: 85
      },
      tableColumn: 1
    },
    fat: {
      title: 'Жиры',
      unit: g,
      norm: {
        boy: 93,
        girl: 85
      },
      tableColumn: 2
    },
    carbohydrate: {
      title: 'Углеводы',
      unit: g,
       norm: {
        boy: 370,
        girl: 340
      },
     tableColumn: 3
    },
    Na: {
      title: 'Na',
      unit: mg,
      norm: {
        boy: 1100,
        girl: 1100
      },
      tableColumn: 4
    },
    K: {
      title: 'K',
      unit: mg,
      norm: {
        boy: 1500,
        girl: 1500
      },
      tableColumn: 5
    },
    Ca: {
      title: 'Ca',
      unit: mg,
      norm: {
        boy: 1200,
        girl: 1100
      },
      tableColumn: 6
    },
    Mg: {
      title: 'Mg',
      unit: mg,
      norm: {
        boy: 350,
        girl: 300
      },
      tableColumn: 7
    },
    P: {
      title: 'P',
      unit: mg,
      norm: {
        boy: 1800,
        girl: 1650
      },
      tableColumn: 8
    },
    Fe: {
      title: 'Fe',
      unit: mg,
      norm: {
        boy: 18,
        girl: 18
      },
      tableColumn: 9
    },
    A: {
      title: 'A',
      unit: mg,
      norm: {
        boy: 1,
        girl: 0.8
      },
      tableColumn: 10
    },
    beta: {
      title: 'beta-каротин',
      unit: mg,
      norm: {
        boy: 5,
        girl: 5
      },
      tableColumn: 11
    },
    B1: {
      title: 'B1',
      unit: mg,
      norm: {
        boy: 1.3,
        girl: 1.3
      },
      tableColumn: 12
    },
    B2: {
      title: 'B2',
      unit: mg,
      norm: {
        boy: 1.5,
        girl: 1.5
      },
      tableColumn: 13
    },
    PP: {
      title: 'PP',
      unit: mg,
      norm: {
        boy: 18,
        girl: 18
      },
      tableColumn: 14
    },
    C: {
      title: 'C',
      unit: mg,
      norm: {
        boy: 70,
        girl: 60
      },
      tableColumn: 15
    },
    energy: {
      title: 'Энергетическая <br/> ценность',
      unit: kkal,
      norm: {
        boy: 2700,
        girl: 2470
      },
      tableColumn: 16
    }  
  };
}
