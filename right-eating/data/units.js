window.units = {
  g: 'г',
  mg: 'мг',
  mcg: 'мкг',
  ml: 'мл',
  kkal: 'ккал'
};
